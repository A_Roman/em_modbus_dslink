package modbus;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.dsa.iot.dslink.node.Node;
import org.dsa.iot.dslink.node.NodeBuilder;
import org.dsa.iot.dslink.node.Permission;
import org.dsa.iot.dslink.node.actions.Action;
import org.dsa.iot.dslink.node.actions.ActionResult;
import org.dsa.iot.dslink.node.actions.EditorType;
import org.dsa.iot.dslink.node.actions.Parameter;
import org.dsa.iot.dslink.node.actions.table.Row;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.node.value.ValueType;
import org.dsa.iot.dslink.serializer.Deserializer;
import org.dsa.iot.dslink.serializer.Serializer;
import org.dsa.iot.dslink.util.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dsa.iot.dslink.util.handler.Handler;

import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.ModbusSlaveSet;
import com.serotonin.modbus4j.ip.tcp.TcpSlave;
import com.serotonin.modbus4j.ip.udp.UdpSlave;

public class ModbusLink {
	static private final Logger LOGGER;

	static {
		LOGGER = LoggerFactory.getLogger(ModbusLink.class);
	}

	static final String ACTION_ADD_LOCAL_SLAVE = "setup local slave";
	static final String ACTION_ADD_IP_CONNECTION = "addIpConnection";

    static final String ACTION_IMPORT = "importConnection";

	static final String ATTRIBUTE_NAME = "name";
	static final String ATTRIBUTE_TRANSPORT_TYPE = "transportType";
	static final String ATTRIBUTE_PORT = "port";
	static final String ATTRIBUTE_SLAVE_ID = "slaveId";
	static final String ATTRIBUTE_RESTORE_TYPE = "restoreType";
    static final String RESULT_SUCCEEDED = "succeeded";

	Node node;
	Serializer serializer;
	Deserializer deserializer;
	private final Map<SlaveNode, ScheduledFuture<?>> futures;
	final Set<ModbusConnection> connections;
	final Set<ModbusMaster> masters;

	static ModbusLink singleton;

	// modbus listener map: port <-> SlaveSet
	private final Map<Integer, ModbusSlaveSet> tcpListeners;
	private final Map<Integer, ModbusSlaveSet> udpListeners;

	boolean restoring = true;

	private ModbusLink(Node node, Serializer ser, Deserializer deser) {
		this.node = node;
		this.serializer = ser;
		this.deserializer = deser;
		this.futures = new ConcurrentHashMap<>();
		this.connections = new HashSet<ModbusConnection>();
		this.masters = new HashSet<ModbusMaster>();

		this.tcpListeners = new HashMap<Integer, ModbusSlaveSet>();
		this.udpListeners = new HashMap<Integer, ModbusSlaveSet>();
	}

	public static void start(Node parent, Serializer copyser, Deserializer copydeser) {

	    // Starts by declaring/creating link root
	    Node node = parent;
		final ModbusLink link = new ModbusLink(node, copyser, copydeser);
		singleton = link;
		link.init();
	}

	public static ModbusLink get() {
		return singleton;
	}

	private void init() {

		restoreLastSession();
		restoring = false;

		// creates the action nodes

        //////////////////////////////////
		//// Add Ip Connection Action ////
		//////////////////////////////////
		Action addIpConnectionAction = new Action(Permission.READ, new AddIpConnectionHandler());

		// add input parameters
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_CONNECTION_NAME, ValueType.STRING));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_TRANSPORT_TYPE, ValueType.makeEnum("TCP", "UDP")));
        addIpConnectionAction.addParameter(new Parameter(IpConnection.ATTR_HOST, ValueType.STRING, new Value("")));
        addIpConnectionAction.addParameter(new Parameter(IpConnection.ATTR_PORT, ValueType.NUMBER, new Value(502)));

        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_TIMEOUT, ValueType.NUMBER, new Value(500)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_RETRIES, ValueType.NUMBER, new Value(2)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_MAX_READ_BIT_COUNT, ValueType.NUMBER, new Value(2000)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_MAX_READ_REGISTER_COUNT, ValueType.NUMBER, new Value(125)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_MAX_WRITE_REGISTER_COUNT, ValueType.NUMBER, new Value(120)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_DISCARD_DATA_DELAY, ValueType.NUMBER, new Value(0)));
        addIpConnectionAction.addParameter(new Parameter(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND, ValueType.makeEnum(ModbusConnection.MULTIPLE_WRITE_COMMAND_OPTIONS), new Value(ModbusConnection.MULTIPLE_WRITE_COMMAND_DEFAULT)));

        // result
        addIpConnectionAction.addResult(new Parameter(RESULT_SUCCEEDED, ValueType.BOOL));

        // set profile name and build
        NodeBuilder addIpConnectionBuilder = node.createChild(ACTION_ADD_IP_CONNECTION, true).setAction(addIpConnectionAction);
        addIpConnectionBuilder.build().setSerializable(false);

        ///////////////////////
        //// Import Action ////
        ///////////////////////
        Action importAction = new Action(Permission.READ, new Handler<ActionResult>(){
            @Override
            public void handle(ActionResult event) {
                handleImport(event);
            }
        });

        // add action parameters
        importAction.addParameter(new Parameter("Name", ValueType.STRING));
        importAction.addParameter(new Parameter("JSON", ValueType.STRING).setEditorType(EditorType.TEXT_AREA));

        // result
        importAction.addResult(new Parameter(RESULT_SUCCEEDED, ValueType.BOOL));

        // set profile name and build
        NodeBuilder importBuilder = node.createChild(ACTION_IMPORT, true).setAction(importAction);
        importBuilder.build().setSerializable(false);

	}

    private void handleImport(ActionResult event) {

        Row resultRow = new Row();
        Value actionResponse;

        // parse responses
        String name = event.getParameter("Name", ValueType.STRING).getString();
        String jsonStr = event.getParameter("JSON", ValueType.STRING).getString();

        JsonObject children = new JsonObject(jsonStr);

        Node child = node.createChild(name, true).build();

        try {

            Method deserMethod = Deserializer.class.getDeclaredMethod("deserializeNode", Node.class, JsonObject.class);
            deserMethod.setAccessible(true);
            deserMethod.invoke(deserializer, child, children);
            ModbusConnection mc = null;
            if (child.getAttribute(ModbusConnection.ATTR_HOST) != null) {
                mc = new IpConnection(this, child);
            } else {
                LOGGER.debug("ERROR: Unknown connection type.");
                //TODO: Throw exception here?
            }
            if (mc != null) {
                mc.restoreLastSession();
            } else {
                child.delete(false);
            }

            actionResponse = new Value(true);

        } catch (SecurityException | IllegalArgumentException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {

            LOGGER.debug("", e);
            child.delete(false);
            actionResponse = new Value(false);

        }

        resultRow.addValue(actionResponse);
        event.getTable().addRow(resultRow);

    }

	public ModbusSlaveSet getSlaveSet(modbus.IpTransportType transtype, int port) {
		ModbusSlaveSet slaveSet = null;

		switch (transtype) {
		case TCP: {
			if (!tcpListeners.containsKey(port)) {
				slaveSet = new TcpSlave(port, false);
				tcpListeners.put(port, slaveSet);
				return slaveSet;
			} else {
				return tcpListeners.get(port);
			}

		}
		case UDP: {
			if (!udpListeners.containsKey(port)) {
				slaveSet = new UdpSlave(port, false);
				return slaveSet;
			} else {
				return this.udpListeners.get(port);
			}

		}
		default:
			return null;
		}
	}

	private void restoreLastSession() {
		if (node.getChildren() == null)
			return;

		Map<String, Node> children = node.getChildren();
		for (Node child : children.values()) {
			Value restype = child.getAttribute(ATTRIBUTE_RESTORE_TYPE);
			if (restype == null) {
				node.removeChild(child, false);
				continue;
			}

			// common parameters of connection
			Value transType = child.getAttribute(ModbusConnection.ATTR_TRANSPORT_TYPE);
			Value timeout = null;
			Value retries = null;
			Value maxrbc = null;
			Value maxrrc = null;
			Value maxwrc = null;
			Value ddd = null;
			Value mw = null;

			if (ModbusConnection.ATTR_RESTORE_CONNECITON.equals(restype.getString())
					|| SlaveFolder.ATTR_RESTORE_FOLDER.equals(restype.getString())) {
				timeout = child.getAttribute(ModbusConnection.ATTR_TIMEOUT);
				if (timeout == null)
					child.setAttribute(ModbusConnection.ATTR_TIMEOUT, new Value(500));
				retries = child.getAttribute(ModbusConnection.ATTR_RETRIES);
				if (retries == null)
					child.setAttribute(ModbusConnection.ATTR_RETRIES, new Value(2));
				maxrbc = child.getAttribute(ModbusConnection.ATTR_MAX_READ_BIT_COUNT);
				maxrrc = child.getAttribute(ModbusConnection.ATTR_MAX_READ_REGISTER_COUNT);
				maxwrc = child.getAttribute(ModbusConnection.ATTR_MAX_WRITE_REGISTER_COUNT);
				ddd = child.getAttribute(ModbusConnection.ATTR_DISCARD_DATA_DELAY);
				mw = child.getAttribute(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND);
				if (mw == null) {
					Value mwo = child.getAttribute("use multiple write commands only");
					String useMW = ModbusConnection.MULTIPLE_WRITE_COMMAND_DEFAULT;
					if (mwo != null && mwo.getBool() != null && mwo.getBool()) {
						useMW = ModbusConnection.MULTIPLE_WRITE_COMMAND_ALWAYS;
					}
					child.setAttribute(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND, new Value(useMW));
					mw = child.getAttribute(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND);
				}

			}


		}
	}

	class AddIpConnectionHandler implements Handler<ActionResult> {

		public void handle(ActionResult event) {

            Row resultRow = new Row();
            Value actionResponse;

            try
            {

                // Get values from action
                Value transportType = event.getParameter(ModbusConnection.ATTR_TRANSPORT_TYPE);
                Value host = event.getParameter(IpConnection.ATTR_HOST);
                Value port = event.getParameter(IpConnection.ATTR_PORT);

                Value name = event.getParameter(ModbusConnection.ATTR_CONNECTION_NAME);
                Value timeout = event.getParameter(ModbusConnection.ATTR_TIMEOUT);
                Value retries = event.getParameter(ModbusConnection.ATTR_RETRIES);
                Value maxReadBitCount = event.getParameter(ModbusConnection.ATTR_MAX_READ_BIT_COUNT);
                Value maxReadRegisterCount = event.getParameter(ModbusConnection.ATTR_MAX_READ_REGISTER_COUNT);
                Value maxWriteRegisterCount = event.getParameter(ModbusConnection.ATTR_MAX_WRITE_REGISTER_COUNT);
                Value discardDataDelay = event.getParameter(ModbusConnection.ATTR_DISCARD_DATA_DELAY);
                Value useMultipleWriteCommandsOnly = event.getParameter(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND);

                if (transportType == null) {
                    transportType = new Value("TCP");
                }

                // Creating the child IP Connection Node
                Node ipConnectionNode = node.createChild(name.toString(), true).build();

                // Adding sttributes
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_TRANSPORT_TYPE, transportType);
                ipConnectionNode.setAttribute(IpConnection.ATTR_HOST, host);
                ipConnectionNode.setAttribute(IpConnection.ATTR_PORT, port);

                ipConnectionNode.setAttribute(ModbusConnection.ATTR_TIMEOUT, timeout);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_RETRIES, retries);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_MAX_READ_BIT_COUNT, maxReadBitCount);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_MAX_READ_REGISTER_COUNT, maxReadRegisterCount);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_MAX_WRITE_REGISTER_COUNT, maxWriteRegisterCount);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_DISCARD_DATA_DELAY, discardDataDelay);
                ipConnectionNode.setAttribute(ModbusConnection.ATTR_USE_MULTIPLE_WRITE_COMMAND, useMultipleWriteCommandsOnly);

                ModbusConnection conn = new IpConnection(getLink(), ipConnectionNode);
                conn.init();

                actionResponse = new Value(true);

            } catch (Exception ex)
            {
                actionResponse = new Value(false);

            }

            resultRow.addValue(actionResponse);
            event.getTable().addRow(resultRow);

		}
	}

	void handleEdit(SlaveFolder slave) {
		Set<Node> set = new HashSet<>(((SlaveNode) slave).getSubscribed());

		for (Node event : set) {
			if (event.getMetaData() == slave) {
				handleUnsub((SlaveNode) slave, event);
				handleSub((SlaveNode) slave, event);
			}
		}
	}

	private void handleSub(final SlaveNode slave, final Node event) {
		slave.addToSub(event);
		if (futures.containsKey(slave)) {
			return;
		}
		ScheduledThreadPoolExecutor stpe = slave.getDaemonThreadPool();
		ScheduledFuture<?> future = stpe.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				slave.readPoints();
			}
		}, 0, slave.intervalInMs, TimeUnit.MILLISECONDS);
		futures.put(slave, future);
	}

	private void handleUnsub(SlaveNode slave, Node event) {
		slave.removeFromSub(event);
		if (slave.noneSubscribed()) {
			ScheduledFuture<?> future = futures.remove(slave);
			if (future != null) {
				future.cancel(false);
			}
		}
	}

	// setup point connection?
	void setupPoint(Node child, final SlaveFolder slave) {
		child.setMetaData(slave);
		child.getListener().setOnSubscribeHandler(new Handler<Node>() {
			public void handle(final Node event) {
				handleSub((SlaveNode) slave, event);
			}
		});

		child.getListener().setOnUnsubscribeHandler(new Handler<Node>() {
			@Override
			public void handle(Node event) {
				handleUnsub((SlaveNode) slave, event);
			}
		});
	}

	private ModbusLink getLink() {
		return this;
	}
}
